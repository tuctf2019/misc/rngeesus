# RNGeesus

## Challenge description
RNGeesus has a secret technique, can you guess it?

## Required files for hosting
`superbadMac.py`

## Hosting instructions
- To run without using our included images, just setup a listener that runs the python script when it gets a connection.

## Hints
- This doesn't use the historical implementation/GNU version. Read carefully!
- Pay attention to what the narrator says -- what's being run on what?
- They need to find the implementation documentation for what the narrator says is running. 
- Keep searching til you find the open-source documentation for the LCG implementation. 

## Flag 
`TUCTF{D0NT_1NS3CUR3LY_S33D_Y0UR_LCGS}`